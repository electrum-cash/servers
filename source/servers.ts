// Import required interfaces.
import type { ElectrumServers } from './interfaces';

// Define the list of available Electrum Cash servers.
export const electrumServers: ElectrumServers =
[
	// Imaginary Usernames servers:
	{
		host: 'bch.imaginary.cash',
		version: '1.5.3',
		transports:
		{
			tcp: 50001,
			tcp_tls: 50002,
			ws:  50003,
			wss: 50004,
		},
	},
	{
		host: 'electrum.imaginary.cash',
		version: '1.5.3',
		transports:
		{
			tcp: 50001,
			tcp_tls: 50002,
			ws:  50003,
			wss: 50004,
		},
	},
];
