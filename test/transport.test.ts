// Load the testing framework.
import { expect, test, vi } from 'vitest';

// Load the electrum library.
import { ElectrumClient } from '@electrum-cash/network';

// Load the server list.
import { electrumServers } from '../source/servers.js';

// Set our default minimum version.
const minVersion = '1.5.1';

/**
 * Tests if a server responds on a given transport scheme.
 *
 * @param {string} host              fully qualified domain name or IP number of the host.
 * @param {number} port              the TCP network port of the host.
 * @param {TransportScheme} scheme   the transport scheme to use for connection
 */
const testServerTransport = async function(host, port, scheme): Promise<void>
{
	// Set up a function to measure failure.
	const failOnError = vi.fn();

	// Execute the test.
	try
	{
		// Initialize an electrum client.
		const electrum = new ElectrumClient('Electrum transport test', minVersion, host, port, scheme);

		electrum.on('error', failOnError);

		// Wait for the client to connect
		await electrum.connect();

		// Close the connection synchronously.
		await electrum.disconnect();
	}
	catch(error)
	{
		// Fail the test due to encountered error.
		failOnError();
	}

	// Verify that there was no failures.
	expect(failOnError).not.toHaveBeenCalled();
};

// Set up transport tests.
const runTransportTests = async function(): Promise<void>
{
	// For each server to test..
	for(const server of electrumServers)
	{
		// .. and for each transport the server supports..
		for(const transport in server.transports)
		{
			// Name the test.
			const title = `Establish connection to ${server.host} with ${transport} transport on port ${server.transports[transport]}`;

			// .. perform a connection test with the server.
			test(title, testServerTransport.bind(null, server.host, server.transports[transport], transport));
		}
	}
};

// Run the transport tests.
runTransportTests();
